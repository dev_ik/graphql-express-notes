const { Sequelize } = require('sequelize');
const constants = require('../constants');

module.exports = new Sequelize(constants.DB_NAME, constants.DB_USER, constants.DB_PASS, {
  host: constants.DB_HOST,
  dialect: 'mysql',
});
