const Todo = require('../models/Todo');
const constants = require('../constants');

module.exports = {
  async getTodos() {
    try {
      return await Todo.findAll()
    } catch (e) {
      throw new Error(`Fetch todos is not avilaibale, ${e.message}`)
    }
  },

  async createTodo({todo}) {
    try {
      return await Todo.create({
        title: todo.title,
        done: false,
      });
    } catch (e) {
      console.log(e);
      throw new Error(constants.MODE === 'prod'
          ? 'Internal Server Error'
          : `Title is required, ${e.message}`)
    }
  },

  async completeTodo({id}) {
    try {
      const todo = await Todo.findByPk(id);
      todo.done = true;
      await todo.save();
      return todo;
    } catch (e) {
      throw new Error(constants.MODE === 'prod'
          ? 'Internal Server Error'
          : `Id is required, ${e.message}`)
    }
  },

  async deleteTodo({id}) {
    try {
      const todo = await Todo.findByPk(id);

      if (!todo) {
        return false;
      }

      await todo.destroy();
      return true;

    } catch (e) {
      throw new Error(constants.MODE === 'prod'
          ? 'Internal Server Error'
          : `Can\'t delete todo, ${e.message}`);
    }
  }
}
