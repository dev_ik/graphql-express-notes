const express = require('express');
const constants = require('./constants');
const path = require('path');
const { graphqlHTTP } = require('express-graphql')
const app = express();
const PORT = process.env.PORT || constants.PORT;
const sequelize = require('./utils/databases');
const schema = require('./graphql/schema');
const resolver = require('./graphql/resolver');

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(graphqlHTTP({
  schema,
  rootValue: resolver,
  graphiql: true
}))
app.use((req, res) => {
  res.sendFile('/index-rest.html');
});

async function start() {
  try {
    await sequelize.sync()
    app.listen(PORT);
  } catch (e) {
    console.log(e);
  }
}

start();
