const {parsed} = require('dotenv').config();

module.exports = {
  DB_HOST: parsed.DB_HOST,
  DB_NAME: parsed.DB_NAME,
  DB_USER: parsed.DB_USER,
  DB_PASS: parsed.DB_PASS,
  PORT: parsed.PORT,
  MODE: parsed.MODE
};
